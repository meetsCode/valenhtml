En este ejercicio vamos a aprender a repetir lo del orden de abrir y cerrar. ¿Te dije ya que el orden de abrir y cerrar se llama anidar? Vamos a ensayar el anidado con otras etiquetas nuevas que no conoces.  

Imagínate que existiera una etiqueta llamada "html" y otra llamada "body" y otra llamada "head"

Si quiero usarlas bien:

```
<html>
  <head>
    Esta es una frase que está dentro de "head"
  </head>
  <body>
    Esta es una frase que está dentro de "body"
  </body>
</html>

```

También lo podría haber escrito así:

```
<html>  <head>  Esta es una frase que está dentro de "head"  </head>  
<body>  Esta es una frase que está dentro de "body"  </body> </html>

```

Pero se lee mucho mejor de la primera manera. Leer una cosa de manera sencilla es importante
para poder encontrar las cosas para cambiarlas. Eso de cambiar las palabras o
las etiquetas lo hacemos programadores constantemente.

En cualquier caso ¿qué podemos aprender de este texto? Podemos ver que dentro de una etiqueta podemos poner varias etiquetas distintas.

















```
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Mi pagina de prueba</title>
  </head>
  <body>
    <img src="images/firefox-icon.png" alt="Mi imagen de prueba">
  </body>
</html>

```





Por último te hablaré de las etiquetas que no necesitan anidarse porque no hay que abrirlas y cerrarlas. Se les llama etiquetas vacías pero bueno, realmente no están tan vacias. ;-)






escribir en orden las etiquetas, bueno, aún más en orden.

¿Te acuerdas cómo escribimos una palabra si la queremos en negrita y cursiva a la vez?

< strong >  < em >    palabras en negrita o en cursiva    < / em > < / strong >
o también
< em >  < strong > palabras en negrita o en cursiva   < / strong > < / em >

Observa como NO se hace:

< strong >  < em > palabras en negrita o en cursiva   < / strong > < / em >

¿Te has dado cuenta de la diferencia?

Es importante que siempre abras y cierres las etiquetas en el orden correcto si usas dos etiquetas a la vez. La primera etiqueta que "abres" es la última que "cierras". A me ayuda a escribir correcto esto así:
1- Abro y cierro una etiqueta:   < strong >     < / strong >
2- Abro y cierro la segunda etiqueta dentro de la primera:  < strong >  < em >        < / em > < / strong >
3- Escribo las palabras dentro de la 2ª etiqueta: < strong >  < em >    palabras en negrita o en cursiva    < / em > < / strong >

Así no me olvido de abrir o cerrar o de ponerlas en el orden adecuado.
