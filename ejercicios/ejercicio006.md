En este ejercicio vamos a aprender a repetir lo del orden de abrir y cerrar. ¿Te dije ya que el orden de abrir y cerrar se llama anidar? Vamos a ensayar el anidado con otras etiquetas nuevas que no conoces.  

Imagínate que existiera una etiqueta llamada "html" y otra llamada "body" y otra llamada "head"

Si quiero usarlas bien:

```
<html>
  <head>
    Esta es una frase que está dentro de "head"
  </head>
  <body>
    Esta es una frase que está dentro de "body"
  </body>
</html>

```

También lo podría haber escrito así:

```
<html>  <head>  Esta es una frase que está dentro de "head"  </head>  
<body>  Esta es una frase que está dentro de "body"  </body> </html>

```

Pero se lee mucho mejor de la primera manera. Leer una cosa de manera sencilla es importante
para poder encontrar las cosas para cambiarlas. Eso de cambiar las palabras o
las etiquetas lo hacemos programadores constantemente.

En cualquier caso ¿qué podemos aprender de este texto? Podemos ver que dentro de una etiqueta podemos poner varias etiquetas distintas.


Mira ahora este texto que hiciste en el ejercicio004 :


<em>Un ejemplo va a ser escribir estos párrafos con etiquetas. <strong>Observa </strong> que todo este párrafo está en cursiva. Pero <strong>también </strong> podemos poner otras etiquetas <strong> cuando lo necesitemos </strong>. Lo único importante es no olvidar cerrar la etiquete con la que empezaste. </em>

¿Cómo es más fácil de programar?
Modo 1:
```
<em>Un ejemplo va a ser escribir estos párrafos con etiquetas. <strong>Observa </strong> que todo este párrafo está en cursiva. Pero <strong>también </strong> podemos poner otras etiquetas <strong> cuando lo necesitemos </strong>. Lo único importante es no olvidar cerrar la etiquete con la que empezaste. </em>
```

Modo 2:
```
<em>
  Un ejemplo va a ser escribir estos párrafos con etiquetas.
  <strong>
    Observa
  </strong>
  que todo este párrafo está en cursiva. Pero
  <strong>
    también
  </strong>
    podemos poner otras etiquetas
  <strong>
    cuando lo necesitemos
  </strong>
  . Lo único importante es no olvidar cerrar la etiquete con la que empezaste.
</em>
```

Puede que el "Modo 2" te parezca un poco raro. Eso es porque aún no te has
encontrado con muchas etiquetas en un texto.
Pero cuando seas una buena programadora te encontrarás con textos muy largos con
un montón de etiquetas y serás feliz al verlos ordenados de la manera 2.
