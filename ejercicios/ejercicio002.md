Ahora que ya sabes escribir con etiquetas vamos a repetir el párrafo del ejercicio 0 con etiquetas en lugar de con asteriscos.

Este texto es para aprender html a través del Markdown. <em>Esta frase está en cursiva</em>. <strong>Esta frase está en negrita</strong>. Las palabras son <strong>negrita</strong> (Bold en inglés) y <em>cursiva</em> (Italic en inglés).

Es muy importante que te acostumbres a usar las etiquetas porque todo el texto en html funciona con etiquetas.  
Recuerda que la forma de escribirlo (que se llama en programación sintaxis) es esta:

< etiqueta >  palabras en negrita o en cursiva   < / etiqueta >

La palabra sintaxis la oirás mucho en programación. Significa cómo se escriben las cosas.
Te voy a poner un ejemplo. La sintaxis de las etiquetas es la que ya te he enseñado:
< etiqueta >  palabras en negrita o en cursiva   < / etiqueta >

Si yo me equivoco y la usara al revés el programa se volvería loco y no sabría cómo escribir las negritas o cursivas. Te pongo un ejemplo de mal uso:
< / etiqueta >  palabras en negrita o en cursiva   <  etiqueta >

¿Qué está mal aquí? Piénsalo antes de seguir leyendo.
Ya te habrás dado cuenta he cambiado la barra (/) de sitio y la he puesto en la primera etiqueta. Eso está mal y hará que todo salga raro en el texto. Pruébalo a ver qué pasa en esta frase:

< / strong >Esta frase pon la etiqueta <  strong  > al revés.<  strong  >
< / strong >Esta frase pon la etiqueta <  strong  > al principio y al final con barra.< / strong  >
< / strong >Esta frase pon la etiqueta <  strong  >  ni al principio ni al final la barra.<  strong  >

Resumiendo: Ahora ya sabes lo que significa la palabra sintaxis (= cómo deben escribirse las cosas)
