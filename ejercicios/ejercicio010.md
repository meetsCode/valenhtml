En este ejercicio es un salto hacia adelante. Ahora solo vamos a usar etiquetas y el resultado de nuestro trabajo tendremos que verlo directamente en un navegador.

Explicar cómo ver un fichero en un navegador.
Sobre recargar con F5.

Un fichero html básico debe tener unas cosas básicas:


```
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Mi pagina de prueba</title>
  </head>
  <body>
    <img src="images/firefox-icon.png" alt="Mi imagen de prueba">
  </body>
</html>

```

Aprovechamos para volver a explicar lo que es la jerarquía y hacer varios diagramas y aprovechar para explicar lo que es un DOM.




Para escribir un párrafo nuevo escribo <  pr  > y al final le pongo lo mismo pero con la barra / es decir: < / pr  >

<pr>
Esta frase debe estar separada de la anterior
</pr>


<pr>
Esta frase y la siguiente deben estar separadas de la anterior párrafo. Esta es la segunda frase que está pegada a la de antes pero separada de los demás.
</pr>

<pr>Esta frase es la última y está también separada.</pr>




Escribir un texto así de seguido es feo. Por eso hay una manera de separar las frases en párrafos. Para escribir un párrafo usaré el sistema de etiquetas. Una etiqueta se usa al principio y al final de lo que se quiere editar.



Escribir un texto así de seguido es feo. Por eso hay una manera de separar las frases en párrafos. Para escribir un párrafo usaré el sistema de etiquetas. Una etiqueta se usa al principio y al final de lo que se quiere editar.


https://platzi.com/blog/faq-todas-tus-preguntas-sobre-platzi-restart-y-platzi-expert/


<pr>
En el ejercicio anterior escribimos este texto todo junto.
Ahora lo queremos escribir con párrafos separados que mejoren la lectura.
</pr>

<pr>
Este texto es para aprender html a través del Markdown. *Esta frase está en cursiva*.</pr>

<pr> **Esta frase está en negrita**. Las palabras son **negrita** (Bold en inglés) y *cursiva* (Italic en inglés).</pr>

<pr>
Escribir un texto así de seguido es feo. Por eso hay una manera de separar las frases en párrafos. </pr>
