Ya hemos avanzado mucho. Vamos a ver las etiquetas vacías y con esto habrás
terminado con los conceptos más básicos del trabajo con etiquetas. En concreto
hoy veremos cómo introducir una foto en el texto.  

Como siempre hay dos maneras de poner una foto. Una es con etiquetas y otra sin ellas.  

Sin etiquetas:  

! [ vacío o no ] ( nombre_del_fichero.png )

Con etiquetas:

< img src="nombre_del_fichero.png" alt="Soy un bocadillo" >

Y ahora toca probar a poner una en este documento. Para ello ya tienes dos imágenes en este directorio. Se llaman "foto-Etiqueta.png" y "box-model-css.png".

La imagen sin etiqueta es esta:  

![foto de Etiqueta](foto-Etiqueta.png )

Y la imagen con etiqueta es esta:  

<img src="box-model-css.png" alt="Soy un montón de cajas">

Como ves, con las etiquetas como estas no hace falta poner una etiqueta al principio y otra al final. Basta con poner una. Por eso no ves una segunda etiqueta que tenga algo así: < / img >  
Otra cosa curiosa es que dentro de la etiqueta ponemos algo más que el nombre de la etiqueta. En este caso el nombre de la etiqueta es "img"

PENDIENTES: explicar lo de los directorios dentro del nombre del fichero.
