En el ejercicio anterior aprendiste lo que significaba la palabra sintaxis. Ahora sabes que significa cómo deben escribirse las cosas. Son las reglas que debes respetar para decir que algo lo has escrito bien.   
Las etiquetas bien escritas tienen esta pinta (o sintaxis)

< etiqueta >  palabras en negrita o en cursiva   < / etiqueta >

Si la palabra "etiqueta" la cambias por "strong" tendrás negritas si la palabra "etiqueta" la cambias por "em" tendrás cursivas.  

Ya te has dado cuenta que la diferencia entre la etiqueta al principio y al final solo es una barra como esta "/"

Cuando la etiqueta no tiene barra (/) va al principio y lo llamamos "abrir etiqueta" cuando va al final lleva barra "/" y lo llamamos "cerrar etiqueta".

Casi todas las etiquetas se abren y luego se cierran. Hay algunas excepciones que veremos más adelante pero de momento lo importante es que cuando alguien te diga "abre una etiqueta de cursiva" debes pensar en escribir < em > y cuando alguien te diga "cierra una etiqueta de cursiva" tu debes escribir esto < / em >.  
No te he dicho nada nuevo porque ya sabes que al abrir una etiqueta va delante y al cerrarla va detrás pero ahora sabes cómo se comunican los informáticos.  
