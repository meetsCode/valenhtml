En este ejercicio veremos las etiquetas que llevan algo más de información para funcionar: Enlace a otra web que le denominan "Vínculos" o "Enlaces".  


También tenemos dos maneras de poner un vínculo a otra web. Una es con etiquetas y otra sin ellas.  

Sin etiquetas:  

`[vacío o no](nombre_del_fichero.html)`
¿Te has dado cuenta? es como poner una imagen pero quitándole el símbolo de cerrar admiración "!"  

Con etiquetas:

`<a href="enlace"> palabra o frase enlazadora </a>`

Y ahora toca probar a poner dos vínculos en este documento. Para ello Usaremos dos webs conocidas por su calidad de educación: `https://developer.mozilla.org/es/docs/Learn/Getting_started_with_the_web/HTML_basics` y `w3schools.com/html/`  

En este [enlace a web de mozilla](https://developer.mozilla.org/es/docs/Learn/Getting_started_with_the_web/HTML_basics) puedes encontrar un **tutorial de html** que es sencillo y de calidad.

Y el enlace con etiqueta es este:  

En el segundo vinculo -en el que usaremos etiqueta- es para la web de <a href="w3schools.com/html/"> w3schools.com </a>  que tiene unos tutoriales muy chulos tanto en html como en SQL o CSS.
