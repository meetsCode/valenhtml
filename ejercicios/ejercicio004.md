En este ejercicio vamos a aprender a escribir en orden las etiquetas, bueno, aún más en orden.

¿Te acuerdas cómo escribimos una palabra si la queremos en negrita y cursiva a la vez?

< strong >  < em >    palabras en negrita o en cursiva    < / em > < / strong >
o también
< em >  < strong > palabras en negrita o en cursiva   < / strong > < / em >

Observa como NO se hace:

< strong >  < em > palabras en negrita o en cursiva   < / strong > < / em >

¿Te has dado cuenta de la diferencia?

Es importante que siempre abras y cierres las etiquetas en el orden correcto si usas dos etiquetas a la vez. La primera etiqueta que "abres" es la última que "cierras". A me ayuda a escribir correcto esto así:
1- Abro y cierro una etiqueta:   < strong >     < / strong >
2- Abro y cierro la segunda etiqueta dentro de la primera:  < strong >  < em >        < / em > < / strong >
3- Escribo las palabras dentro de la 2ª etiqueta: < strong >  < em >    palabras en negrita o en cursiva    < / em > < / strong >

Así no me olvido de abrir o cerrar o de ponerlas en el orden adecuado.


<em>Un ejemplo va a ser escribir estos párrafos con etiquetas. <strong>Observa </strong> que todo este párrafo está en cursiva. Pero <strong>también </strong> podemos poner otras etiquetas <strong> cuando lo necesitemos </strong>. Lo único importante es no olvidar cerrar la etiquete con la que empezaste. </em>


<strong>Este otro párrafo estará todo en negrita. Pero algunas <em>palabras </em> también estarán en <em>cursiva</em>. Siempre y cuando no te hayas olvidado de cerrar correctamente el párrafo anterior. </strong>
