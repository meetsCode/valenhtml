En este ejercicio aprenderemos a escribir negritas y cursivas con etiquetas.


Este texto es para aprender html a través del Markdown y etiquetas. *Esta frase está en cursiva con 1 asterisco*. **Esta frase está en negrita con 2 asteriscos**. Las palabras son **negrita** (Bold en inglés) y *cursiva* (Italic en inglés).  

Una forma de que algunas palabras aparezcan con negrita o con cursiva es escribiendo una etiqueta antes y después de las palabras En el caso de las negritas se usa la etiqueta <  strong  > y en el caso de cursiva   <  em  >. La segunda debe llevar el símbolo / (barra) después del < .

Quedarán así:
<strong> **palabra** </strong>
 y
<em> *palabra* </em>

Prueba a escribir esta frase correctamente: Esta frase tiene palabras en <strong>negrita</strong> otras en <em>cursiva</em> y otras con </em><strong>ambas</strong></em>.  

¿Ya lo tienes? Felicidades.

En el siguiente ejercicio vamos a repasar esta lección para tenerlo aún más claro.
